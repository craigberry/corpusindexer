# CorpusIndexer

CorpusIndexer generates a lucene-based BlackLab index for adorned TEI
files created by MorphAdorner. BlackLab is a corpus indexing and search
facility developed by the Dutch Language Institute. For more
information about BlackLab, please see

        http://inl.github.io/BlackLab/

CorpusIndexer is a custom BlackLab indexer designed to process adorned
TEI files produced by MorphAdorner. For more information about
MorphAdorner, please see the MorphAdorner site at:

        http://morphadorner.northwestern.edu

Current CorpusIndexer version: 2.0.6. Last update: April 5, 2022.

The CorpusIndexer source code and support files, along with an issue
tracker, are available as a git repository on bitbucket.org at

        http://bitbucket.org/pibburns/corpusindexer


File Layout of CorpusIndexer Release
------------------------------------

	|File or Directory       |Contents                                        |
	|------------------------|------------------------------------------------|
	|build.xml               |The Ant build file used to build CorpusIndexer. |
	|indexer.properties      |Indexing properties.                            |
	|ivy.xml                 |Apache Ivy dependencies definitions.            |
	|ivysettings.xml         |Apache Ivy settings.                            |
	|lib/                    |Java library files used by CorpusIndexer.       |
	|                        |These are retrieved on demand during the        |
	|                        |build process using Apache Ivy.                 |
	|license.txt             |The CorpusIndexer license.                      |
	|modhist.txt             |CorpusIndexer modification history.             |
	|README.md               |Printable copy of this file in Windows markdown |
	|                        |format (lines terminated by Ascii cr/lf).       |
	|createindex             |Unix shell command file to create a BlackLab    |
	|                        |index from adorned TEI files.                   |
	|createindex.bat         |Windows batch file to create a BlackLab index   |
	|                        |from adorned TEI files.                         |
	|addtoindex              |Unix shell command file to add adorned TEI files|
	|                        |to an existing BlackLab index.                  |
	|addtoindex.bat          |Windows batch file to add adorned TEI files     |
	|                        |to an existing BlackLab index.                  |
	|deletefromindex         |Unix shell command file to delete adorned TEI   |
	|                        |files from an existing BlackLab index.          |
	|deletefromindex.bat     |Windows batch file to delete adorned TEI files  |
	|                        |from an existing BlackLab index.                |
	|searchindex             |Unix shell command file to search a             |
	|                        |BlackLab index created from adorned TEI files.  |
	|searchindex.bat         |Windows batch file to search a BlackLab index   |
	|                        |created from adorned TEI files.                 |
	|src/                    |CorpusIndexer source code.                      |


Installing and Building CorpusIndexer
-------------------------------------

To build the CorpusIndexer code, make sure you have installed recent
working copies of a Java Development Kit and Apache Ant on your
system (at least Java 1.8 or later). The Java development kits for
Windows, Mac OS X, and Linux systems may be obtained from:

        http://www.oracle.com/technetwork/java/javase/downloads/index.html

Alternatively, you can use OpenJDK from:

        http://openjdk.java.net/install/index.html

You can also use Amazon Coretto:

        https://aws.amazon.com/corretto/

You can obtain Apache Ant from

        http://ant.apache.org

Move to the directory into which you cloned a local copy of the
repository for CorpusIndexer.

Open a console or terminal window, move to the base directory of the
CorpusIndxer release, and type:

        ant

to build CorpusIndexer. If the build completes successfully, the
corpusindexer.jar file will be placed in the "dist" subdirectory.

Type

        ant javadoc

to generate the javadoc (internal documentation) into subdirectory
"javadoc".

Type

        ant clean

to remove the effects of compilation. This does not remove the
downloaded files in the lib subdirectory. To remove those as well, type

        ant cleanlib

Once in a while, if you are having trouble compiling, you may need to
clean your Ivy cache to make sure you have the correct library files.
Type

        ant cleancache

to clean the Ivy cache.


Running CorpusIndexer
---------------------

On Unix-like systems you should make the indexing and search scripts
executable using the chmod command.

        chmod 755 createindex
        chmod 755 addtoindex
        chmod 755 deletefromindex
        chmod 755 searchindex

The Windows batch files will operate as they stand.

To create a new BlackLab index from a collection of adorned files, use
the command:

         createindex indexdir inputfilesdir     (Windows)
        ./createindex indexdir inputfilesdir    (Unix)

Here "indexdir" is the name of the output directory in which to create
the BlackLab index. ANY EXISTING BLACKLAB INDEX IN THAT DIRECTORY WILL
BE DELETED!

"inputfilesdir" specifies the name the input directory containing the
adorned files to index.

The optional "options" can include: 
	
	--maxdocs n Stop after indexing n documents. 
	--indexparam file Read properties file file with parameters for 
	  the indexer. By default, if the current directory, the input
      directory, or the index directory (or their parents) contains a file
      named indexer.properties, the values in that file are passed to the
      indexer. 
    --name value Pass a parameter named "name" with value "value"
      to the indexer.

Example:

        createindex /myindex /myfiles

creates a BlackLab index in directory "/myindex" for the adorned TEI XML
files in directory "myfiles".

To add (or replace) files in an existing index, use the command:

        addtoindex indexdir [options] inputfilesdir      (Windows)
        ./addtoindex indexdir [options] inputfilesdir    (Unix)

New files will also be added to the index. Files with the same name as an
existing file in the index will replace those existing files.

Example:

        addtoindex /myindex /myotherfiles

adds the adorned TEI XML files in directory "/myotherfiles" to the index
in "/myindex". Files with the same names as files already in the index
will be replaced by the versions in "/myotherfiles".

To delete files from an existing index, use the command:

        deletefromindex indexdir deletequery

Here "deletequery" specifies a Lucene query to select the files to
delete from the index.

Example:

        deletefromindex /myindex title:hamlet

deletes all files with "hamlet" in the title from the index contained in
directory "/myindex".

As distributed, the indexing commands above use a Java virtual machine
with a memory size of two gigabytes. Some large corpora (for example,
EarlyPrint) may require more memory to process. You can change the -mx2048m 
command line parameter in the script/batch files to a larger value if needed.  
For the EarlyPrint corpus, we usually use a memory size of 32 gigabytes (-mx32g).


Indexer properties file
-----------------------

The indexer.properties file contains settings which control the way the
indexer works.  The settings as distributed in indexer.properties are
those used by the EarlyPrint project.  Several are different from the
default values listed below.

Currently the following properties are available.

        IndexPunctuation=true|partial|false

                Select true to index punctuation for searching.
                Select partial to index end of sentence punctuation only.
                The default is false (punctuation is not indexed
                for searching).  Set to partial for EarlyPrint texts.

        IndexSynonyms=true|false

                Select true to index synonyms specified by the syn=
                attribute on <w> and <pc> elements if present.
                The default is false.

        IndexWordIDs=true|partial|false

                Select true to index word IDs specified by the xml:id
                attribute in <w> and <pc> elements.  Select partial to
                index only the part of the word ID following the
                document ID.  The default is false (word IDs are not 
                indexed).  Indexing full page IDs significantly slows the 
                indexing process, and may cause indexing to fail for 
                larger text collections.  Set to partial for EarlyPrint texts. 

        IndexPageIDs=true|false
        
                Select true to index page IDs specified by the
                xml:id attribute in ancestral <pb> elements.  
                The default is false (page IDs are not indexed). 
                Indexing page IDs significantly slows the indexing 
                process.  Set to true for EarlyPrint.
                
        UnknownPartOfSpeech=zz

                Specifies the value representing an unknown part of
                speech in the part of speech tag set used in the adorned
                TEI files.  The default is zz which is the value for the
                NUPOS tag set.

        AllowFullContentViewing=true|false

                Specifies that the full original content of the
                stored document can be viewed by client programs.
                The default is true.

        IndexSpeakers=true|false

                Select true to add a speaker to each word element.
                The speaker is taken from the nearest ancestral <sp> 
                element.  The default is false.  Indexing speakers slows
                indexing signifcantly.  Set to true for EarlyPrint 
                texts.

        IgnorableDivTypes=list-of-blank-separated-div-types

                Specifies <div> element types whose content should not be
                indexed.  By default the content of all div elements
                will be indexed.  For EarlyPrint, set this to
                
                	machine-generated_castlist textual_notes

        UseCreationDateAsPublicationDate=true|false

                When both a creation date and a publication date
                appear in the TEI header, use the creation date
                for both dates.  Default is false.  Set to true
                for EarlyPrint.

        AuthorDatePattern1=regular expression to match author name
        AuthorDatePattern2=second pattern applied after first
        AuthorDatePattern3=third pattern applied after second, etc.

                Patterns for matching dates in author fields.

                Each is applied repeatedly, in order.  The first match
                group is assumed to contain the author name text.
                The remaining text is ejected from the author name.

		StoreShortFileName=true|false
		
			When true, stores a short file name as the document indetifier.
			When false, an opaque internal is used.
			The default is true.
			
		UseNuHeader=true|false
		
			Use the obsolete nuHeader for some metadata values.
			Default is false.
			
		UeMonkHeader=true|false
		
			Use the obsolete Monk header for some metadata values.
	        Default is false.
			
		useEpHeader=true|false
		
			When true, uses the contents of the <xenoData type="epHeader"> section
			to set values for several fields such as curator name, etc.  
			Default is false.  Set to true for EarlyPrint texts.
			
		IndexReversedStandardSpellings=true|false

			When true, indexes the reversed standard spelling as the "revreg" attribute.
			Default is false.  Set to true for EarlyPrint if you prefer.
			
		IndexReversedWords=true|false

			When true, indexes the reversed spelling as the "revword" attribute.
			Default is false.  Set to true for EarlyPrint is you prefer.

				
Searching indexed files
-----------------------

You can use the searchindex script (Unix) or batch file (Windows) to
search indexed corpora.

        searchindex indexdir      (Windows)
        ./searchindex indexdir    (Unix)

where "indexdir" is the directory containing a BlackLab index. This can
be any BlackLab index, not just one created from MorphAdorned TEI files.

Example:

        searchindex /myindex

searches the index in directory "/myindex". A search such as

        [lem="king"] [word="of"] [pos="np.*"]

will look for any form of the lemma "king" followed by the spelling "of"
followed by an proper noun, assuming the NUPos part of speech tag set
was used in the files indexed.


License
-------

CorpusIndexer is licensed under an NCSA style open source license. See
the license.txt file for details.

The BlackLab library is licensed under an Apache 2.0 license. Libraries
used by BlackLab (including Lucene) have their own licenses as well.

        |Library              |License                    |
        |---------------------|---------------------------|
        |Ant                  |Apache 2.0                 |
        |Blacklab             |Apache 2.0                 |
        |Byte Buddy           |Apache 2.0                 |
        |Commons Compress     |Apache 2.0                 |
        |Commons CSV          |Apache 2.0                 |
        |Commons FileUpload   |Apache 2.0                 |
        |Commons IO           |Apache 2.0                 |
        |Commons Lang3        |Apache 2.0                 |
        |Commons Text         |Apache 2.0                 |
        |Eclipse Collections  |Eclipse public license v1.0|
        |FastUtil             |Apache 2.0                 |
        |Hamcrest             |BSD 2-clause               |
        |Ivy                  |Apache 2.0                 |
        |Jackson Annotations  |Apache 2.0                 |
        |Jackson Core         |Apache 2.0                 |
        |Jackson Databind     |Apache 2.0                 |
        |Jackson Dataformat   |Apache 2.0                 |
        |JCIP Annotations     |Public                     |
        |Json                 |JSON                       |
        |JUnit                |Eclipse public license v1.0|
        |Log4j                |Apache 2.0                 |
        |Lucene 5.5.2         |Apache 2.0                 |
        |Mockito              |MIT license                |
        |Net Bytebuddy        |Apache 2.0                 |
        |Snakeyaml            |Apache 2.0                 |

